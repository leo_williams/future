To run the task 1 test navigate to the root dir and execute:
- task_1_fizzBuzz_test/FizzBuzz.php
- check the log file that gets created

To run the task 2 test navigate to the root dir and execute:
- php artisan serve
- navigate to http://127.0.0.1:8000/
- file located in public/app.js

To run the task 3 test navigate to the root dir and execute:
- task_3_parser_test/FileParser.php