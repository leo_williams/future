<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>

    </head>
    <body class="antialiased">
        <div class="col-md-10 mx-auto mt-5">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Merchant logo</th>
                        <th>Merchant name</th>
                        <th>Product name</th>
                        <th>Product price</th>
                        <th>Product affiliate link</th>
                    </tr>
                </thead>
                <tbody>
                    {{--JS will fill--}}
                </tbody>
            </table>
        </div>
        <footer>
            <!--bootstrap js-->
            <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
            <script type="application/javascript" src="{{ asset('assets/js/app.js') }}"></script>
        </footer>
    </body>
</html>
