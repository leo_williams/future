<?php

/**
 * Class FizzBuzz
 */
class FizzBuzz
{
    /**
     * Find a prime number
     *
     * @param int $number
     * @return bool
     */
    function isPrime(int $number): bool
    {
        // 1 is not prime
        if ( $number == 1 ) {
            return false;
        }
        // 2 is the only even prime number
        if ( $number == 2 ) {
            return true;
        }
        // square root algorithm speeds up testing of bigger prime numbers
        $x = sqrt($number);
        $x = floor($x);
        for ( $i = 2 ; $i <= $x ; ++$i ) {
            if ( $number % $i == 0 ) {
                break;
            }
        }

        if( $x == $i-1 ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * FizzBuzz
     *
     * @param int $number
     */
    public function fizzBuzzz(int $number): void
    {
        $fizz = 'Fizz';
        $buzz = 'Buzz';
        $logData = '';

        if ($number >= 1) {
            for ($i = 0; $i < $number; $i++) {
                if (($i % 3) === 0) {

                    $logData .= $fizz . PHP_EOL;

                    print $fizz . PHP_EOL;

                } else if (($i % 5) === 0) {

                    $logData .= $buzz . PHP_EOL;

                    print $buzz . PHP_EOL;

                } else if (($i % 3) === 0 && ($i % 5) === 0) {

                    $logData .= $fizz . $buzz . PHP_EOL;

                    print $fizz . $buzz . PHP_EOL;

                } else if ($this->isPrime($i) === true) {

                    $logData .= $fizz . $buzz . '++' . PHP_EOL;

                    print $fizz . $buzz . '++' . PHP_EOL;
                }
            }

            $this->log($logData);
        }
    }

    /**
     * Log the data
     *
     * @param string $data
     */
    public function log(string $data): void
    {
        $file = fopen("fizzbuzz.log", 'ab') or die("Unable to open file!");

        $txt = $data;
        fwrite($file, $txt);

        fclose($file);

        print 'Log successful' . PHP_EOL . PHP_EOL;

    }
}

$number = 500;
$fizzBuzz = new FizzBuzz;

$fizzBuzz->fizzBuzzz($number);