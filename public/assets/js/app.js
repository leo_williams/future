$(document).ready(e => {
    $.get('http://search-api.fie.future.net.uk/widget.php?id=review&site=TRD&model_name=iPad_Air')
        .done(response => {
            let offers = response.widget.data.offers;
            let $table = $('.table');

            if (undefined !== offers) {
                // Print offers
                $.each(offers, (id, offer) => {
                    $table.find('tbody')
                        .append($('<tr>' +
                            '<th scope="row"> <img src="' + offer.merchant.logo_url +'" width="100px"/></th>' +
                            '<th>' + offer.merchant.name +'</th>' +
                            '<th>' + offer.offer.name +'</th>' +
                            '<th>' + offer.offer.currency_symbol + offer.offer.price +'</th>' +
                            '<th>' + offer.offer.link +'</th>' +
                            '</tr>'));
                });
            }
        })
});