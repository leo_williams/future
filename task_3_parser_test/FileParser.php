<?php

/**
 * Class FileParser
 *
 * @package FilePaser
 */
class FileParser
{
    /**
     * Tags groups
     *
     * @var array
     */
    protected $tagGroups;

    /**
     * Directory for scanning
     *
     * @var string
     */
    protected $directory;

    /**
     * FileParser constructor.
     *
     * @param array $tagGroups
     * @param string $directory
     */
    public function __construct(array $tagGroups, string $directory)
    {
        $this->tagGroups = $tagGroups;
        $this->directory = $directory;
    }


    /**
     * Parse the data
     *
     * @param string $file
     */
    public function parser(string $file = ''): void
    {
        $dontInclude = [
            '.',
            '..',
            '.DS_Store',
            'appCodes.ini',
        ];

        $dir = __DIR__ . '/' . $this->directory;

        if (is_dir($dir)) {
            if ($directory = opendir($dir)) {
                while (($file = readdir($directory)) !== false) {
                    if (!in_array($file, $dontInclude, true)) {

                        if (is_dir($file)) {

                            $this->parser($file);

                        } else {
                            $headers = [
                                'id',
                                'appCode',
                                'deviceId',
                                'contactable',
                                'subscription_status',
                                'has_downloaded_free_product_status',
                                'has_downloaded_iap_product_status'
                            ];

                            // Open/Create the file
                            $f = fopen($dir . '/' .basename($dir . '/' . $file) . '_Csv.csv', 'ab');

                            // Write to the csv
                            fputcsv($f, $headers);

                            // Close the file
                            fclose($f);
                        }
                    }
                }
                closedir($directory);
            }
        }
    }
}

$tagGroups = [
    'subscription_status' => [
        'active_subscriber',
        'expired_subscriber',
        'never_subscribed',
        'subscription_unknown',
    ],
    'has_downloaded_free_product_status' => [
        'has_downloaded_free_product',
        'not_downloaded_free_product',
        'downloaded_free_product_unknown',
    ],
    'has_downloaded_iap_product_status' => [
        'has_downloaded_iap_product',
        'not_downloaded_free_product',
        'downloaded_iap_product_unknown',
    ]
];

$directory = 'parser_test';

$fizzBuzz = new FileParser($tagGroups, $directory);

$fizzBuzz->parser();